package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class EchoTests {

	@Test
	public void testResourceInitialisation() {
		EchoResource resource = new EchoResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationRetrieveTestPathNoSecurity() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveTestPath(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationRetrieveTestPathNotAuthorised() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.retrieveTestPath(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationRetrieveTestPathAuthorised() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.retrieveTestPath(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationRetrieveNoSecurity() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(null);

		Response response = resource.retrieve();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationRetrieveNotAuthorised() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.retrieve();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationRetrieveAuthorised() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.retrieve();
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateNameNoSecurity() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(null);

		Response response = resource.createName(null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateNameNotAuthorised() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createName(null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateNameAuthorised() {
		EchoResource resource = new EchoResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createName(null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}